![Banner](materiales/banner.png)

# Física IV B (B5330) - Astrofísica y Cosmología (B5300) 

**Asignatura de la carrera de Profesorado en Física [Universidad Nacional de Río Negro](http://www.unrn.edu.ar/ "UNRN")**

**Dr. Hernán Asorey**
***Departamento Física Médica, Centro Atómico Bariloche, Av. E. Bustillo 9500, (8400) San Carlos de Bariloche, Argentina***

Este curso ha sido movido a un nuevo repositorio en Github. Visítanos en este enlace: [github.com/asoreyh/unrn-f4b](https://github.com/asoreyh/unrn-f4b).


# Licencia

**Física IV B (IPAC: Introducción a Partículas, Astrofísica y Cosmología)**, (c) por Hernán Asorey, 2016, 2017, 2018, 2019, 2020, 2021.
<br /><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
<br />Este trabajo se distribuye en forma gratuita bajo la licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es">Licencia Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a>. Debería haber recibido una copia de la licencia junto con este trabajo [cc-by-nc-sa-40-legalcode.txt](https://gitlab.com/asoreyh/unrn-ipac/blob/master/cc-by-nc-sa-40-legalcode.txt). En su defecto, visite <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es">http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es</a>.
